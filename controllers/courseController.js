const Course = require("../models/Course");
const User = require('../models/User');
const bcrypt = require("bcrypt");
const auth = require("../auth");
// Controller Functions:

// Creating a new course
/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if(error) {
			return false 
		} else {
			return true
		}
	})
};*/

// s39 refactored addCourse Controller
module.exports.addCourse = (userData, reqBody) => {
	return User.findById(userData.id).then(result => {
		if(result == null) {
			return false
		} else if(result.isAdmin){
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			return newCourse.save().then((course, error) => {
				if(error) {
					return false 
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})

};
	


// Retreiving All Courses
module.exports.getAllCourses = (data) => {

	if(data.isAdmin) {
		
		return Course.find({}).then(result => {

			return result
		})
	} else {
		return false //"You are not and Admin."
	}
};

// Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result
	})
};

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
};

//Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
		console.log(updatedCourse)
		if(error) {
			return false
		} else {
			return true
		}
	}) 
};

//Archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse, error) => {
		console.log(archivedCourse)

		if(error) {
			return false
		} else {
			return true
		}
	})
};



